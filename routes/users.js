var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.render('user/list.html', {
        title : 'My First Nunjucks Page',
        message : 'My First Nunjucks Page',
        items : [
            { name : 'item #1' },
            { name : 'item #2' },
            { name : 'item #3' },
            { name : 'item #4' },
        ]
    });
});


router.post("/", function (req, res, next) {
    res.send("POSTED OK")
});

module.exports = router;
