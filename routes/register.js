/**
 * Created by mr bean on 2017-03-30.
 */
var express = require("express");
var router  =  express.Router();
var UserModel = require("../models/user");

router.get("/", function (req, res, next) {
    res.render("register.html", {title:"Register new account"})
});

router.post("/", function (req, res, next) {
    var username = req.body.email;
    var password = req.body.password;
    var password2 = req.body.password2;
    var message = "OK";

    var user_model = new UserModel();
    user_model.email = username;
    user_model.password = password;

    if(password != password2){
        message = "Password missmatched";
    }



    user_model.save(function (err) {
            if(err){
                throw err
            }
            message = "Password OK, creating login soon";
        });

    res.render("register.html", {title:"Register new account",
    message:message})
});


module.exports = router;